/// pondscum.js - Guilhem Doulcier 2016 - GPLv3
/// Require d3.js

var fmt = d3.format('.0%')
var fmt_freq = d => d ? '(Freq: '+fmt(d)+')' : ''
var fmt_float =     d3.format('2f')

const SYMBOLS = ` <symbol id="nosm">
	     <circle cy="18.969" cx="18.571" r="15.268" fill="#0088aa"/>
	     <path d="m0.40179 0.7997 36.339 36.339" stroke="#f00" stroke-width="1px" fill="none"/>
	     <path d="m36.741 0.7997-36.339 36.339" stroke="#f00" stroke-width="1px" fill="none"/>
	     </symbol>
	     <symbol id="nows"> <path d="m36.83 19.42c-8.036 1.354-0.284 3.182-0.7888 4.826-1.175 4.035-14.05 4.7-8.658 7.121 4.83 2.168-11.96-1.762-7.478 1.077 6.357 4.024-19.24-3.431-13.21-3.855 3.279-0.2306-10.6-7.387-3.732-6.352 3.356 0.5057-2.578-9.71 0.04184-7.455s3.599-6.978 4.717-4.825c1.933 3.723 4.906-9.447 6.118-3.418 1.212 6.028 12.48-5.753 11.65-1.314-1.634 8.765 12.61 3.231 9.803 5.255-9.905 7.136 6.452 8.114 1.536 8.942z" fill="#d4aa00"/><path d="m0.3536 0.3535 36.34 36.34" stroke="#ff0000" stroke-width="1px" fill="none"/><path d="m36.69 0.3535-36.34 36.34" stroke="#ff0000" stroke-width="1px" fill="none"/></symbol>
	     <symbol id="nomat"><rect height="17.68" width="18.06" y="19.37" x="9.257" fill="#afdde9"/>  <path d="m12.1 21.88c-0.5014-1.097-0.4659 0.08126-0.7203 0.0721-0.622-0.01333-1.21-1.829-1.35-0.9651-0.125 0.7737-0.2063-1.778-0.4404-1.028-0.3318 1.063-0.2468-2.883 0.0452-2.037 0.1586 0.46 0.6501-1.799 0.7652-0.7769 0.0562 0.4992 1.29-0.7406 1.068-0.2798-0.2221 0.4609 1.136 0.2472 0.8707 0.4895-0.4583 0.419 1.539 0.3396 0.7232 0.7439-0.8155 0.4043 1.301 1.564 0.6342 1.615-1.316 0.1022 0.0213 1.927-0.3758 1.603-1.4-1.143-0.9129 1.233-1.22 0.5623z" stroke="#000000" stroke-width=".1" fill="#d4aa00"/><path d="m27.49 0.7052v36.32h-18.44v-36.32" stroke="#000000" stroke-miterlimit="79.4" stroke-width=".6145" fill="none"/><g stroke="#000000" stroke-width=".1" fill="#d4aa00"><path d="m15.71 19.45c-1.19 0.2004-0.0421 0.4711-0.1168 0.7144-0.1739 0.5974-2.08 0.6957-1.282 1.054 0.715 0.321-1.771-0.2609-1.107 0.1594 0.941 0.5957-2.849-0.5079-1.955-0.5707 0.4854-0.03413-1.569-1.093-0.5524-0.9402 0.4967 0.07486-0.3816-1.437 0.006-1.104 0.3878 0.3338 0.5328-1.033 0.6982-0.7143 0.2861 0.5511 0.7262-1.398 0.9057-0.506 0.1794 0.8923 1.847-0.8516 1.724-0.1945-0.2419 1.297 1.867 0.4782 1.451 0.7778-1.466 1.056 0.9551 1.201 0.2275 1.324z"/><path d="m19.06 19.54c-1.19 0.2004-0.0421 0.4711-0.1168 0.7144-0.1739 0.5974-2.08 0.6957-1.282 1.054 0.715 0.321-1.771-0.2609-1.107 0.1594 0.941 0.5957-2.849-0.5079-1.955-0.5707 0.4854-0.03413-1.569-1.093-0.5524-0.9402 0.4967 0.07486-0.3816-1.437 0.006-1.104 0.3878 0.3338 0.5328-1.033 0.6982-0.7143 0.2861 0.5511 0.7262-1.398 0.9057-0.506 0.1794 0.8923 1.847-0.8516 1.724-0.1945-0.2419 1.297 1.867 0.4782 1.451 0.7778-1.466 1.056 0.9551 1.201 0.2275 1.324z"/><path d="m22.18 19.77c-1.19 0.2004-0.0421 0.4711-0.1168 0.7144-0.1739 0.5974-2.08 0.6957-1.282 1.054 0.715 0.321-1.771-0.2609-1.107 0.1594 0.941 0.5957-2.849-0.5079-1.955-0.5707 0.4854-0.03413-1.569-1.093-0.5524-0.9402 0.4967 0.07486-0.3816-1.437 0.006-1.104 0.3878 0.3338 0.5328-1.033 0.6982-0.7143 0.2861 0.5511 0.7262-1.398 0.9057-0.506 0.1794 0.8923 1.847-0.8516 1.724-0.1945-0.2419 1.297 1.867 0.4782 1.451 0.7778-1.466 1.056 0.9551 1.201 0.2275 1.324z"/><path d="m24.32 17.26c0.5584 1.069 0.461-0.1056 0.7155-0.1098 0.6219-0.01934 1.305 1.763 1.399 0.8929 0.0842-0.7792 0.2994 1.765 0.4938 1.003 0.2755-1.079 0.3978 2.866 0.0618 2.036-0.1825-0.451-0.5547 1.83-0.7234 0.816-0.0824-0.4955-1.249 0.8073-1.051 0.3355 0.1976-0.472-1.147-0.1873-0.8952-0.4431 0.4356-0.4425-1.554-0.2583-0.7613-0.7049 0.7931-0.4465-1.381-1.493-0.7181-1.58 1.309-0.1711-0.1224-1.923 0.2911-1.62 1.458 1.068 0.8469-1.28 1.188-0.6256z"/><path d="m26.56 19.9c-1.19 0.2004-0.0421 0.4711-0.1168 0.7144-0.1739 0.5974-2.08 0.6957-1.282 1.054 0.715 0.321-1.771-0.2609-1.107 0.1594 0.941 0.5957-2.849-0.5079-1.955-0.5707 0.4854-0.03413-1.569-1.093-0.5524-0.9402 0.4967 0.07486-0.3816-1.437 0.006-1.104 0.3878 0.3338 0.5328-1.033 0.6982-0.7143 0.2861 0.5511 0.7262-1.398 0.9057-0.506 0.1794 0.8923 1.847-0.8516 1.724-0.1945-0.2419 1.297 1.867 0.4782 1.451 0.7778-1.466 1.056 0.9551 1.201 0.2275 1.324z"/><path d="m24.15 19.77c-1.19 0.2004-0.0421 0.4711-0.1168 0.7144-0.1739 0.5974-2.08 0.6957-1.282 1.054 0.715 0.321-1.771-0.2609-1.107 0.1594 0.941 0.5957-2.849-0.5079-1.955-0.5707 0.4854-0.03413-1.569-1.093-0.5524-0.9402 0.4967 0.07486-0.3816-1.437 0.006-1.104 0.3878 0.3338 0.5328-1.033 0.6982-0.7143 0.2861 0.5511 0.7262-1.398 0.9057-0.506 0.1794 0.8923 1.847-0.8516 1.724-0.1945-0.2419 1.297 1.867 0.4782 1.451 0.7778-1.466 1.056 0.9551 1.201 0.2275 1.324z"/></g><path d="m0.3536 0.3536 36.34 36.34" stroke="#ff0000" stroke-width="1px" fill="none"/><path d="m36.69 0.3536-36.34 36.34" stroke="#ff0000" stroke-width="1px" fill="none"/></symbol>
`
const SCALE_DIFF = d3.scaleOrdinal(d3.schemeCategory10)
const SCALES = {
    // Scales for different variables. Used in the colour of the HTML table. 
    'mutations': d3.scaleOrdinal(d3.schemeCategory20),
    'fail': d=>{var colors = {0:'#4e79a7',1:'#e0585b',2:'#f18f3b',3:'#59a14e'}; return colors[d]?colors[d]:'#bab0ac'},
    'rack': d3.scaleOrdinal(d3.schemeCategory10),
    'ancestor': d3.scaleOrdinal(d3.schemeCategory10),
    'survival': d3.scaleLinear().domain([0,1]).range(['red','green']),
    'survival1': d3.scaleLinear().domain([0,1]).range(['#cccccc','#4e79a7']),
    'survival2': d3.scaleLinear().domain([0,1]).range(['#cccccc','#f28e2b']),
    'survival_diff': d=> +d!=0?SCALE_DIFF(d):'#cccccc',
    'prop': d3.scaleLinear().domain([0,1]).range(['#ff7f00', '#1776b6' ]),
    'sx': d3.scaleLinear().domain([0,1]),
    'sy': d3.scaleLinear().domain([0,1]),
    'phase': d=>+d==1?'#d4aa00' : '#0088aa',
    'mat':d=>+d?'#1776b633':undefined,
    'count_sm':d=>+d?'#1776b633':undefined,
    'count_ws':d=>+d?'#1776b633':undefined,
    'count_fz':d=>+d?'#1776b633':undefined,
    'extinct':d=> +d?'#1776b6' : '#ff7f00',
}

function type_of_fail(d){
    if(d.phase == 1 && d.mat==0){return 1}//
    else if (d.phase == 1 && d.mat == 1 && d.count_sm == 0) {return 2} // 
    else if (d.phase == 2 && d.count_ws == 0) {return 3}// 
    else if (d.extinct == 0) {return 0}
    else{return 4}
}
const FAIL_MESSAGE = {1:'No mat or collapsed mat', 2:'No smooth cells', 3:'No wrinkly cells', 0:"Survived", 4:'Unknown'}

const THEMES  = {
    // Color Themes for the tree. 
    phase:  {
	color_node: function(d) {return d.data.phase == 1? '#d4aa00' : '#0088aa'},
	color_link: function(d) {return d.parent.data.phase == 1 ? '#0088aa' : '#d4aa00'}
    },
    rack:
    {
	color_node: function(d) {return SCALES.rack(d.data.rack)},
	color_link: function(d) {return SCALES.rack(d.parent.data.rack)}
    },
    survival_estimate:
    {
	color_node: function(d) {return d.data.phase==1?SCALES.survival1(d.data.survival_estimate):SCALES.survival2(d.data.survival_estimate)},
	color_link: function(d) {return d.data.phase==1?SCALES.survival1(d.data.survival_estimate):SCALES.survival2(d.data.survival_estimate)}
    },
    survival_diff:
    {
	color_node: function(d) {return SCALES.survival_diff(d.data.survival_diff)},
	color_link: function(d) {return SCALES.survival_diff(d.data.survival_diff)}
    },
    ancestral_rack:
    {
	color_node: function(d) {return SCALES.rack(d.data.ancestor.rack)},
	color_link: function(d) {return SCALES.rack(d.parent.data.ancestor.rack)}
    },
    status:
    {
	color_node: function(d) {return SCALES.fail(type_of_fail(d.data))},
	color_link: function(d) {return SCALES.fail(type_of_fail(d.data))}
    },

}

function add_icons(nodes, table){
    // Add a symbol next to the node. 
    nodes.filter(d => +table[d.data.name].extinct)
	.append('use')
	.attr('xlink:href', function(name){
	    var d = table[name.data.name]
	    if(d.phase == 1 && d.mat==0){return '#nomat'}
	    else if (d.phase == 1 && d.mat == 1 && d.count_sm == 0) {return '#nosm'}
	    else if (d.phase == 2 && d.count_ws == 0) {return '#nows'}
	    else if (d.extinct == 0) {return ''}
	    else{ return '#somethingelse'}})
	.style('transform','scale(0.4)')
	.attr('x', '10')
	.attr('y', -25)
}


function describe(node, title){
    // Describe a node (used in the tooltip)
    var desc = '<h3>'+title+'</h3> <span class="badge badge-default" style="background-color:'+SCALES['rack'](node.rack)+'">Tube : '+node.tube+'</span> '
    desc = desc + '<span class="badge badge-default"> Phase: '+ node['phase'] +' Cycle: '+ node.cycle +'</span>'
    desc = desc + '<table  style="width:100%" class="table-condensed table-bordered">'
	+'<th>Mat</th><td> '+(node.mat?'Yes':'No')+'</td></tr>'
	+'<th>WS</th><td> '+node.count_ws+' ('+fmt((+node.count_ws)/((+node.count_sm)+(+node.count_ws)))+')</td></tr>'
	+'<th>SM</th><td> '+node.count_sm+' ('+fmt((+node.count_sm)/((+node.count_sm)+(+node.count_ws)))+')</td></tr>'
	+'<th>Fz</th><td> '+node.count_fz+'</td></tr>'
	+'</table>'
    desc = desc + (+node.extinct ?
		   ('<span class="badge badge-danger"> Extinct </span> '+FAIL_MESSAGE[type_of_fail(node)]+', replaced by cells from tube '+node.replaced+'.<br/>' )
		   :'<span class="badge badge-success"> Survived </span>')
    desc = desc + (node.condition ? ('<em>Condition : '+node.condition+'</em><br/>') : '')
    return desc
}

function barplot(table, max_depth, svg){
    /// Barplot in the subpanel.
    /// Table is the list of selected nodes
    /// max_depth is the maximum depth of the tree
    /// svg is the DOM element (as d3js selection) in which to plot. 

    // Set scales
    var x = SCALES.sx
    var y = d3.scaleLinear().domain([0,1]).range([20,200])
    var barWidth = (x.range()[1]-x.range()[0])/max_depth
    
    // Count the causes of failures per depth (probably too convoluted). 
    var nested = d3.nest()
	.key(function(d) { return d.depth; }).sortKeys(d3.ascending)
	.key(function(d) { return type_of_fail(d); }).sortKeys(d3.descending)
	.rollup(function(v) { return v.length; })
	.entries(table)

    // Compute cumulative sums for the stacked bar plots.
    nested.forEach(d=>{
	var s = 0
	var sn = 0
	d.total = d3.sum(d.values.map(e=>e.value))
	d.values.forEach(e=>{
	    e.depth=+d.key
	    e['prop'] = e.value/d.total
	    e['cumsum_prop'] = s+e.prop
	    e['cumsum'] = sn + e.value
	    s = s+e.prop
	    sn = sn+e.value
	    return e})
    })

    // Clear Svg
    svg.html('')

    // Plot a stack-bar per depth. 
    var bar_d = svg.selectAll('g').data(nested)
    var bar = bar_d.enter()
	.append('g')
	.attr('transform', function(d, i) { return 'translate(' + (+d.key) * barWidth +  ',0)' })
    bar_d.exit().remove()

    // Number of nodes in each slice. 
    bar.append('text')   
	.attr('y', y(0)-1)
	.attr('x', barWidth/2-0.5)
	.style('text-anchor', 'middle')
	.text(d=>d.values[d.values.length-1].cumsum)

    // Plot each slice. 
    slicebar = bar.selectAll('rect')
	.data(d=>d.values)
	.enter()
	.append('rect')
        .on('mouseout', tooltip_remove(TOOLTIP))
	.on('mousemove', tooltip_move(TOOLTIP))
	.on('mouseover', function(d){
	    TOOLTIP.style('display', 'block')
	    TOOLTIP.select('div').html('<h3>'+FAIL_MESSAGE[d.key]+'</h3>'
				       +'<strong>Number: </strong>'+d.value + ' ('+fmt(d.prop)+')<br/>')
	})
	.attr('height', e=> y(e.prop))
	.attr('width', barWidth - 1)
	.attr('x', 0)
	.attr('y', e=>y(e.cumsum_prop-e.prop))
        .style('fill', d=>SCALES.fail(d.key))
    slicebar.exit().remove()
}
