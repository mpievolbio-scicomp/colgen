/// main.js - Collective Genealogy interactive visualisation 
// This file is part of the coalgen package.
// Copyright 2019 Guilhem Doulcier, Licence GNU GPL3+
/// Require d3.js and some functions declared in another script (default pondscum.js)

///// Globals////
var DATASET_LIST

// important divs
var TABLE
var SVG
var TOOLTIP
var KEYS
var MUT

// plot parameters
var W = 1 , H = 3
var SEPNODE_SIB = 8
var SEPNODE = 10
var RNODE = '6'

// Load the list of datasets and prepare the selector.
d3.json('dataset_list')
    .then(({tree_and_data:list}) => {
	DATASET_LIST = list
	SVG = d3.select('main svg')
	TOOLTIP = d3.select('#tooltip')
	TABLE = d3.select('#table')
	console.log(list)

	var win_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var win_h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

	d3.select('main')
	    .style('height',(win_h-100)+'px')
	    .style('width', win_w+'px')
	    .style('overflow','scroll')

	d3.select('#plot svg')
	    .style('width', win_w+'px')

	var s = d3.select('#select').append('select')
	s.selectAll('option')
	    .data(list)
	    .enter()
	    .append('option')
	    .text(d => d)
	s.on('change', function(){setup(DATASET_LIST[this.selectedIndex],
					SVG)})

	var s2 = d3.select('#select_theme').append('select')
	s2.selectAll('option').data(d3.keys(THEMES)).enter()
	    .append('option').attr('value', d => d).html(d => d).property('selected', d=>d=='phase')

	var panel_control = d3.select('#panel_control')
	var table_div = d3.select("#table")
	var plot_div = d3.select("#plot")
	var mutation_div = d3.select("#mutations")
	var config_div = d3.select("#config")

	var ctrl = config_div.append('form')
	ctrl.append('label').text('Width:')
	ctrl.append('input').attr('value', W)
	    .on('change', function(){W=this.value;
				     setup(DATASET_LIST[s.node().selectedIndex],
					   SVG)})
	ctrl.append('label').text('Height:')
	ctrl.append('input').attr('value', H)
	    .on('change', function(){H=this.value;
				     setup(DATASET_LIST[s.node().selectedIndex],
					   SVG)})
	ctrl.append('label').text('Node size:')
	ctrl.append('input').attr('value', RNODE)
	    .on('change', function(){RNODE=this.value;
				     setup(DATASET_LIST[s.node().selectedIndex],
					   SVG)})
	setup(DATASET_LIST[0], SVG)

	panel_control
	    .append('button')
	    .text('table').on('click', d=>{table_div.style('display','block')
					   plot_div.style('display','none')
					   mutation_div.style('display','none')
					   config_div.style('display','none')
					  })
	panel_control
	    .append('button')
	    .text('plot').on('click', d=>{plot_div.style('display','block')
					  table_div.style('display','none')
					  mutation_div.style('display','none')
					  config_div.style('display','none')})
	panel_control
	    .append('button')
	    .text('mutations').on('click', d=>{plot_div.style('display','none')
					       table_div.style('display','none')
					       mutation_div.style('display','block')
					       config_div.style('display','none')})
	    .attr('id','mutation_button')
	panel_control
	    .append('button')
	    .text('config').on('click', d=>{plot_div.style('display','none')
					       table_div.style('display','none')
					       mutation_div.style('display','none')
					       config_div.style('display','block')})
	    .attr('id','mutation_button')

	panel_control.append('span').attr('id','describe_selection').style('margin-left','2em')
    }, console.error)

function setup(name, svg){
    console.log('SETUP', name)

    // Reset the svg
    var win_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    var win_h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

    width = W * (19/20) * win_w
    height = H * (4/5)* win_h
    svg.html(SYMBOLS).attr('width',width).attr('height',height)

    var tree_layout = d3.tree()
	.size([height-10*RNODE, width-2*RNODE])
	.separation(function(a, b) {return a.parent == b.parent ? SEPNODE_SIB : SEPNODE})

    SCALES.sy.range([(4/5)*win_h-10*RNODE, 10*RNODE])
    SCALES.sx.range([0, (18/20)*win_w])

    tree_file = '/tree/'+name
    data_file = '/table/'+name
    mut_file = '/mutations/'+name

    // Load the dataset and tree, and start plotting.
    Promise.all([d3.json(tree_file), d3.csv(data_file), d3.csv(mut_file)])
	.then(function(alldata){
	    console.log('LOADED', alldata)
	    var tree = d3.hierarchy(alldata[0])
	    var mut = alldata[2]
	    if (mut.length==0){d3.select("#mutation_button").style('display','none')}
	    else{
		d3.select("#mutation_button").style('display','')
		try {
		    mut.forEach(d =>{ d.nodes = JSON.parse(d.nodes)})
		}
		catch(e) {
		    console.log('Mutation loading failed',e)
		}
	    }
	    var table = {}
	    MUT = mut

	    // Put the csv into a tree.
	    alldata[1].forEach(d => table[d.name] = d)
	    KEYS = d3.keys(table)

	    // Copy the data from the table into the tree.
	    tree.descendants().forEach(e => {
		e.data = Object.assign(e.data, table[e.data.name])
		if (e.data.name in table){table[e.data.name].depth = e.depth
					 }
	    })
	    var max_depth = d3.max(KEYS.map(d=>table[d].depth))

	    // Adjusting scales (should be moved out of here). 
	    var d1 = [d3.min(tree.descendants().filter(d=>d.data.phase==1), d=>+d.data.survival_estimate),
		      d3.max(tree.descendants().filter(d=>d.data.phase==1), d=>+d.data.survival_estimate)]
	    SCALES.survival1.domain(d1)
	    var d2 =[d3.min(tree.descendants().filter(d=>d.data.phase==2), d=>+d.data.survival_estimate),
		     d3.max(tree.descendants().filter(d=>d.data.phase==2), d=>+d.data.survival_estimate)]
	    SCALES.survival2.domain(d2)

	    // Compute the ancestor of each node.
	    tree.descendants()
		.filter(d=>d.depth==1)
		.forEach(
		    d=>{
			d.descendants()
			.forEach(e=>{e.data.ancestor=d.data
				     table[e.data.name].ancestor = d.data})
		       })


	    // Clicking anywhere on the SVG reset the state of the tree.
	    svg.on('click', d=>{document.dispatchEvent(new CustomEvent('select_nodes'))})

	    // Theme selector change the colour of the nodes.
	    var themeselect = d3.select('#select_theme select')
		.on('change', function(){colorize_nodes(svg, tree, THEMES[this.options[this.selectedIndex].value])})

	    document.addEventListener("select_nodes", function(event){

		// SELECT NODES
		let is_selected = {}

		if (event.detail  && 'nodes' in event.detail && event.detail.nodes.length){
		    tree.descendants()
			.forEach(e => {
			    if(e.depth){
				var active = event.detail.nodes.includes(e.data.name)
				e.data.active = active
				is_selected[e.data.name]= active
			    }
		    })}
		else{
		    tree.descendants().forEach(function(e){e.data.active=true; is_selected[e.data.name]=true})}

		// Remove eventual duplicates.
		let selection = d3.keys(is_selected).filter(d=>is_selected[d])

		total = selection.length

		// UI UPDATE
		d3.select('#describe_selection').html(total+' node'+(total>1?'s':'')+' selected')
		opacity_nodes(SVG,tree)
		barplot(selection.filter(d=>d in table).map(d=>table[d]), max_depth, d3.select('#plot svg'))
		TABLE.select('tbody').selectAll('tr').data(KEYS).style('display', d=> is_selected[d]?'':'none')
	    })

	    /// FILL THE PAGE ///
	    // Node table in its tab
	    fill_table(table, TABLE)

	    // Mutation table in its tab
	    if (mut.length!=0){filter_muts = fill_mut_table(mut, d3.select('#mutations'), tree)}

	    // Plot the tree and colorise nodes
	    plot(tree, table, svg, TOOLTIP, tree_layout)
	    colorize_nodes(svg, tree, THEMES[themeselect.node().options[themeselect.node().selectedIndex].value])

	    // Select all nodes.
	    document.dispatchEvent(new CustomEvent('select_nodes'))
	}, console.error)
}

function fill_table(dataset, div){
    // Fill the HTML table containing the nodes.
    div.html('')
    d3.select('#describe_selection').html(describe_selection)
    var columns = d3.keys(dataset[KEYS[0]]).filter(d=>d!='ancestor')
    var table = div.append('table')
	.attr('class','table table-sm')
	.style('display',' block')
	.style('height','200px')
	.style('overflow', 'scroll')
    var head = table.append('thead').append('tr')
    columns.forEach(function(e){head.append('th').attr('scope','col').text(e)})
    var body = table.append('tbody')
    var rows = body.selectAll('tr').data(KEYS).enter().append('tr')
    columns.forEach(function(e){
	rows.append('td')
	    .text(d=>dataset[d][e])
	    .style('background-color', d=>(SCALES[e]?SCALES[e](dataset[d][e]):undefined))})
 }

function fill_mut_table(data, div, tree){
    // Fill the HTML table containing the mutations
    div.html('')
    data.sort((a,b)=>+a.number<+b.number?1:-1)
    var html = ['<table class="table table-sm" style="display:block;height:200px;layout:fixed;overflow:scroll">']
    var keys = d3.keys(data[0]).filter(e=>(e!='name'&&e!='gene'&&e!='nodes'))
    html.push('<thead><th>Gene</th><th>'+keys.map(d=>d).join('</th><th>')+'</th></thead><tbody>')
    var nodes = {}
    var colour = d3.scaleOrdinal(d3.schemePastel1)
    data.forEach(d=>{
	nodes[d.name] = d.nodes
	html.push('<tr id="'+d.name+'"><td style="background-color:'+colour(d.pattern)+';">'+d.gene+'</td><td>'+keys.map(e=>d[e]).join('</td><td>')+'</td></tr>')
    })
    html.push('</tbody></table>')
    div.html(html.join(''))

    function filter_by_node(node_list){
	var lines = div.select('tbody').selectAll('tr')
	var count = 0
	if (node_list.length){
	    var filt = d=>{
		let is_present = node_list.filter(node=> nodes[d.getAttribute('id')].includes(node)).length
		d.style.display = is_present ? 'table-row':'none'
		count += is_present ? 1:0
	    }
	    lines.nodes().forEach(filt)
	}
	else{lines.nodes().forEach(d=>{count++; d.style.display='table-row'}) }
	d3.select('#describe_selection').append('span').html(' ('+count+' mutations)')
    }

    document.addEventListener('select_nodes', event=>{
	if (event.detail  && 'nodes' in event.detail && event.detail.nodes.length){
	    filter_by_node(event.detail.nodes)}
	else{filter_by_node([])}
    })


    div.node().addEventListener("click", function(e) {
	id = e.target.parentNode.getAttribute('id')
	document.dispatchEvent(new CustomEvent('select_nodes', {'detail': {'nodes':nodes[id]}}))
    })


    return filter_by_node
}


function plot(tree, table, svg, tooltip, tree_layout){
    // Plot the nodes to the svg. Add click callbacks.

    console.log('Plotting ', tree.data.name)

    var posx = d => d.y
    var posy = d => d.x
    
    // Add the links
    var links = svg.append('g')
    links.selectAll('.link')
	.data(tree_layout(tree).descendants().filter(d => !d.parent?0:d.parent.depth) )
	.enter().append('path')
	.attr('class', 'link')
	.attr('d', function(d) {
	    return 'M' + (posx(d)-(+RNODE)) + ',' + posy(d) //Move to the focal node.
		+ 'C' + (posx(d) + posx(d.parent)) / 2 + ',' + posy(d) // Curve to the mean in y and the child x
		+ ' ' + (posx(d) + posx(d.parent)) / 2 + ',' + posy(d.parent) // Curve to and the child y (polybezier)
		+ ' ' + (posx(d.parent)+(+RNODE)) + ',' + posy(d.parent)
	})
	.style('fill', 'none')
	.style('stroke-width', '5')

    // Add the node groups that contains labels and circle and icon.
    var last_click = undefined
    var consecutive_click = 0
    var node = svg.selectAll('.node')
	.data(tree.descendants().filter(function(d){return !d.parent?0:1}))
	.enter().append('g')
	.on('mouseout', tooltip_remove(tooltip))
	.on('mousemove', tooltip_move(tooltip))
	.on('mouseover', function(d){
	    tooltip.style('display', 'block')
	    tooltip.select('div').html(describe(table[d.data.name], d.data.name))
	})
	.on('click', function(d){
	    if (last_click == d){
		consecutive_click++
		if(consecutive_click>=4){consecutive_click=0}
	    }
	    else {consecutive_click=0}
	    last_click = d
	    var nodelist
	    switch(consecutive_click){
	    case 0:
		nodelist = d.descendants().map(d=>d.data.name)
		nodelist = nodelist.concat(d.ancestors().map(d=>d.data.name))
		break
	    case 1:
		nodelist = d.descendants().map(d=>d.data.name)
		break
	    case 2:
		nodelist = d.ancestors().map(d=>d.data.name)
		break
	    case 3:
		nodelist = [d.data.name]
		break
	    }
	    d3.event.stopPropagation()
	    document.dispatchEvent(new CustomEvent('select_nodes', {'detail':{'nodes':nodelist}}))
	})
	.attr('class', function(d) {return 'node' + (d.children? ' node--leaf' : ' node--internal') })
	.attr('transform', function(d) { return 'translate(' + posx(d) + ',' + posy(d) + ')' })

    // Add the node circles.
    node.append('circle')
	.attr('r', RNODE)

    // Add the extinction icons icons.
    add_icons(node, table)

    // Add the node label
    node.append('text')
	.attr('dy', 3)
	.attr('x', 16)
	.style('text-anchor', 'end')
	.style('opacity', function(d) { return d.children ? .1 : 1 })
	.text(function(d) { return table[d.data['name']].tube })
}



function opacity_nodes(g, tree){
    // Upadte the opacity of the nodes according to the selection
    // Filter the table according to the selection.
    g.selectAll('.node')
	.data(tree.descendants().filter(function(d){if(!d.parent){return 0} return 1}))
	.style('opacity', function(d) { return (d.data.active ? 1 : .20) })
    g.selectAll('.link')
	.data(tree.descendants().filter(function(d){if(!d.parent){return 0} return d.parent.depth}))
	.style('opacity', function(d) { return ((d.data.active && d.parent.data.active) ? 1 : .33) })
}

function colorize_nodes(g, tree, theme){
    // Color the nodes and links according to the current theme.
    console.log('update', theme)
    g.selectAll('.node')
	.data(tree.descendants().filter(function(d){if(!d.parent){return 0} return 1}))
	.select('circle')
	.style('fill', theme.color_node)
    g.selectAll('.link')
	.data(tree.descendants().filter(function(d){if(!d.parent){return 0} return d.parent.depth}))
	.style('stroke', theme.color_link)
	.style('fill','none')
	g.selectAll('.link')
}

function tooltip_move(tooltip) {
    return function(){
	var bb = tooltip.node().getBoundingClientRect()
	var win_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var win_h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	tooltip.style('top', Math.min(win_h-bb.height, (d3.event.y + 10)) + 'px')
	    .style('left', Math.min(win_w-bb.width, (d3.event.x + 10)) + 'px')
    }
}

function tooltip_remove(tooltip) {
    return function(){tooltip.style('display', 'none')}
}
