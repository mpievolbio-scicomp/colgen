"""mutations.py - A mutation mapping method by BN,
This file is part of the coalgen package.
Copyright 2019 Guilhem Doulcier, Licence GNU AGPL3+
"""
import json
import logging

import pandas as pd
import numpy as np

import colgen.tree as tree
import colgen.dataimport

logger = logging.getLogger('coalgen')

def generate_potential(N, evidence, roots=(0,), p = (.1,.9), mu=.01, mu_reverse=None):
    ''' Generate the potentials K_i(X_i, X_pa(i))

    Args:
        N: (int) Number of nodes
        roots: (iterable) idx Root nodes
        p: (iterable) emission probability p[k] = P(E_i=1|X_i=k)
        mu: (float) mutation probability 0->1.
        mu_reverse: (float) mutation probability 1->0.

    Returns:(np.array) Potential[i,k,j] = K_i(k,j)
    '''
    D = len(p)
    potential = np.zeros((N, D, D))
    #logger.warning('Generate potential. {} Nodes.'.format(N))

    # emission probability
    pemm = np.zeros((2,2)) # pemm[i,j] := P(E = j | X = i)
    pemm[0,:] = [1-p[0], p[0]] # P(E | X = 0)
    pemm[1,:] = [1-p[1], p[1]] # P(E | X = 1)

    # Reverse mutation.
    mu_r = mu if mu_reverse is None else mu_reverse

    if type(evidence) == list:
        evidence = {i:x for i,x in enumerate(evidence)}

    for i in range(N):
        #print(i, i in evidence)
        if i in evidence:
            e = evidence[i]
            if i not in roots:
                a =  np.array([[(1-mu)*pemm[0,e], mu_r*pemm[0,e]],
                               [ mu * pemm[1,e]  , (1-mu_r)*pemm[1,e]]])
                #print(e, a.shape, a)

                # K(X_i, X_pa(i)) = P(X_i = k | X_pa(i) = j) P(E_i=e_i | X_i=k)
                potential[i,:,:] = np.array([[(1-mu)*pemm[0,e], mu_r*pemm[0,e]],
                                            [ mu * pemm[1,e]  , (1-mu_r)*pemm[1,e]]])
            else:
                # K(X_i) = P(E_i=e_i | X_i=k)
                a =np.array([[pemm[0,e], pemm[0,e]],
                             [pemm[1,e], pemm[1,e]]])
                #print(a.shape)
                potential[i,:,:] = np.array([[pemm[0,e], pemm[0,e]],
                                            [pemm[1,e], pemm[1,e]]])
        else:
            # K(X_i, X_pa(i)) = P(X_i = k | X_pa(i) = j)
                potential[i,:,:] = np.array([[(1-mu), mu],
                                            [ mu, (1-mu)]])
    return potential


def max_sum_ve(potential, order, parent):
    products = np.zeros_like(potential)
    potential = np.log(potential)
    for i, children in order:
        potential, products = max_sum_eliminate_variable(potential, products, i ,children)
    try:
        map_assignment = traceback_map(products,order,parent)
    except Exception as ex:
        print('MAP FAILED:', ex)
        raise
        return ex, products
    return map_assignment,  potential[0,0,0]

def max_sum_eliminate_variable(potential, product, i, children):
    '''Fill the message[i]'''
    #print('Eliminate the variable {}, children: {}'.format(i, children))
    # Combine all the factors that contain i in their scope. Remove them.
    # sum of child factors + factor of i.
    product[i,:,:] = potential[i,:,:] + np.sum([potential[j,:,:] for j in children], axis=0)
    #print('sum:\n {} \n {}'.format(np.exp(np.sum([potential[j,:,:] for j in children],axis=0)),
    #                               np.exp(potential[i,:,:])))
    #print('product:\n {}'.format(np.exp(product[i,:,:])))

    # Max-marg the resulting factor and add it to the list of factors.
    # M_i(X_pa(i)) = max_{Xi} prod[Xi , x_pa(i)]
    potential[i,0,:] = np.max(product[i,:,0])
    potential[i,1,:] = np.max(product[i,:,1])

    #print('Max-marg:\n {}'.format(np.exp(potential[i,:,:])))

    #potential[i,0,:] = np.sum(product[i,:,0])
    #potential[i,1,:] = np.sum(product[i,:,1])

    # The factor product is kept for the backtracking step.
    # The factor maximisation replace the factor of the eliminated variable.
    return potential, product

def traceback_map(products, order, parent):
    map_assignment = [np.nan] * (len(order))
    for i,_ in order[::-1]:
        try:
            if i in parent:
                map_assignment[i] = np.argmax(products[i,:,map_assignment[parent[i]]])
            else:
                map_assignment[i] = np.argmax(products[i,:,0])
        except Exception as ex:
            print("Exception:{}\n i:{}\n parent[i]:{}\n map_assignment[parent[i]]:{}".format(ex, i,parent[i], map_assignment[parent[i]]))
            raise ex
    return map_assignment

def sum_product_ve(potential, states, order, parent):
    """Perform the sum product with variable elimination"""
    products = np.zeros_like(potential)
    potential = np.log(potential)
    s = 0
    for i,_ in order:
        if i in parent:
            s += potential[i,states[i],states[parent[i]]]
        else:
            s += potential[i,states[i],0]
    return s

def main(node_table, mutation_table, mu, mu_r, p0, p1):
    node_table = node_table.copy() # Prevent in place modification of the node table.
    
    # Compute the elimination order on the tree.
    data = colgen.dataimport.table_to_json(node_table, 'root', dump=False)
    tree.set_elimination_order(data)
    odict = {node:order-1 for order, node in tree.get_attr(data, 'name').items()}
    node_table['order'] = [odict[n] for n in node_table.name]
    all_nodes = frozenset(list(odict.keys()))

    # Build an ordered increasing list of (order, order of children).
    # Used by the max_product algorithm.
    order = [(odict[x], tuple(node_table.query('parent=="{}"'.format(x)).order.values))
             for x in node_table.sort_values('order', ascending=True).name]

    # Build a mapping child node (order)->parent node (order).
    parent = {odict[x]: odict[y]
              for x, y in zip(node_table.name, node_table.parent)
              if y in odict}

    nnode = len(odict)

    # Establish which nodes where sequenced and which nodes are not.
    if 'sequenced' in node_table.columns:
        sequenced = frozenset(node_table.query('sequenced==1').name)
    else:
        logger.warning('No column "sequenced", assuming that collectives in the mutation list were sequenced')
        sequenced = frozenset(mutation_table.node.unique()).intersection(all_nodes)
        node_table['sequenced'] = [x in sequenced for x in node_table.name]
    logger.info('{} mutations / {}/{} nodes sequenced'.format(mutation_table.mutation.nunique(),
                                                              len(sequenced), len(all_nodes)))

    report = []
    for mut, nodes in mutation_table.groupby('mutation'):

        # Generate potential based on sequenced nodes.
        bear_it = set(nodes.node)
        evidence = {odict[x]:int(x in bear_it) for x in sequenced}
        potential = generate_potential(nnode,
                                       evidence,
                                       roots=[nnode],
                                       p=(p0, p1),
                                       mu=mu,
                                       mu_reverse=mu_r)

        # Compute maximum likelihood mapping.
        config, proba = max_sum_ve(potential, order, parent)

        # Output
        report.append({
            'mutation':mut,
            'number':int(np.sum(config)),
            'number_seq':np.sum([int(x) for x in evidence.values()]),
            'confidence':np.exp(proba),
            'mu':mu,
            'mu_reverse':mu_r,
            'p0':p0,
            'p1':p1,
            'nodes': json.dumps([n for n, i in zip(node_table.name, node_table.order) if config[i]])
        })
        node_table[mut] = [config[i] for i in node_table.order]

    # Output
    columns = ['name', 'sequenced']+[mut for mut in mutation_table.mutation.unique()]
    node_table = node_table[columns].set_index('name')
    report = pd.DataFrame(report)
    return report, node_table
