"""bn_discrete.py - A small Bayesian network library
This file is part of the coalgen package.
Copyright 2019 Guilhem Doulcier, Licence GNU GPL3+
"""
import time
from functools import partial
from collections import defaultdict
from collections import Counter
import logging

import numpy as np
import scipy.optimize
import scipy.integrate

logger = logging.getLogger('coalgen')
DEBUG = True


class Potential:
    def __init__(self, scope, function=None, name='Potential', default_domain=None, letter='K', domain_var=None, mx=False):
        self.name = name
        self.scope = scope
        self.letter = letter

        if default_domain is not None:
            self.domain_var = defaultdict(lambda: default_domain)
        else:
            self.domain_var = {}
        self.domain_grid = {}

        if mx:
            self.argmax = np.nan
            self.argmax_pos = np.nan

        if domain_var is not None:
            for var, dom in domain_var.items():
                self.domain_var[var] = dom

        self.domain = np.meshgrid(*([self.domain_var[x] for x in scope]), indexing='ij')
        if function:
            self.function = np.vectorize(function)
            self.value = self.function(*self.domain)
        else:
            if self.domain:
                self.value = np.zeros_like(self.domain[0])
            else:
                self.domain = None
                self.value = 0

    def __repr__(self,):
        try:
            return "{}({}) <{}>-><{}>".format(self.letter, ','.join(['X{}'.format(x) for x in self.scope]),  str(self.domain[0].shape)[1:-1], str(self.value.shape)[1:-1], )
        except (AttributeError, TypeError):
            return "{}({}) {}->{}".format(self.letter, ','.join(['X{}'.format(x) for x in self.scope]), self.domain, self.value)

    def plot(self, ax=None, levels=None, continuous=True, text=False):
        """Draw potential value"""
        import matplotlib.pyplot as plt
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        if ax is None:
            ax = plt.gca()
        fig = plt.gcf()
        if len(self.scope) == 2:
            if continuous:
                img1 = ax.contourf(
                    self.domain_var[self.scope[0]], self.domain_var[self.scope[1]], self.value, levels=levels)
                ax.set(
                    xlabel=self.scope[0], ylabel=self.scope[1], title='{}'.format(self))

            else:
                img1 = ax.imshow(self.value,
                                 interpolation=None,
                                 extent=[0,
                                         len(self.domain_var[self.scope[1]]),
                                         0,
                                         len(self.domain_var[self.scope[0]])],
                                 origin='lower')
                ax.set(xticks=np.arange(len(self.domain_var[self.scope[1]]))+0.5, xticklabels=self.domain_var[self.scope[1]],
                       yticks=np.arange(len(self.domain_var[self.scope[0]]))+0.5, yticklabels=self.domain_var[self.scope[0]])
                ax.set(xlabel='X{} <{}>'.format(self.scope[1], len(self.domain_var[self.scope[1]])),
                       ylabel='X{} <{}>'.format(self.scope[0], len(
                           self.domain_var[self.scope[0]])),
                       title='{}'.format(self))

            if text:
                for i, x in enumerate(np.arange(len(self.domain_var[self.scope[1]]))+0.5):
                    for j, y in enumerate(np.arange(len(self.domain_var[self.scope[0]]))+0.5):
                        ax.text(
                            x, y, self.value[j, i], horizontalalignment='center', verticalalignment='center')
            divider = make_axes_locatable(ax)
            cax1 = divider.append_axes("right", size="5%", pad=0.05)
            fig.colorbar(img1, cax=cax1)
        if len(self.scope) == 1:
            if continuous:
                ax.plot(self.domain_var[self.scope[0]], self.value)
            if not continuous:
                ax.bar(self.domain_var[self.scope[0]], self.value)

            ax.set(xlabel=self.scope[0],
                   ylabel='Value', title='{}'.format(self))
        return ax


def max_product_variable_elimination(factors, ordering, backtrack=True):
    """Variable elimination algorithm for maximum a posteriori inference."""
    message = {}
    for i in ordering:
        factors, message[i] = max_product_eliminate_variable(factors, i)
    if DEBUG:
        logger.debug('----- Factors: ' +
                     '\n'.join(['{}'.format(x) for x in factors]))
        logger.debug(
            '----- Messages:'+'\n'.join(['{}'.format(message[x]) for x in ordering[::-1]]))
    if backtrack:
        map_values = traceback_map(
            ordering[::-1], [message[j] for j in ordering[::-1]])
    else:
        map_values = None

    if map_values and DEBUG:
        values = list(map_values.values())
        logger.debug('-- MaxProduct Elim --')
        logger.debug('Average state {} std state: {}, min:{} max: {}'.format(np.mean(values),
                                                                             np.std(
                                                                                 values),
                                                                             np.min(
                                                                                 values),
                                                                             np.max(values)))
        logger.debug('Some values: {}'.format(Counter(values)))
        logger.debug('-'*10)
    return map_values, factors, [message[j] for j in ordering[::-1]]


def factor_sum(factors):
    """Return a factor that is the sum of factors. The new scope is the union of all scopes."""
    scopes = [f.scope for f in factors]
    scope = sorted(list(set().union(*scopes)))
    domain_var = {}
    for f in factors:
        for k, v in f.domain_var.items():
            domain_var[k] = v
    sumpot = Potential(scope, letter='S', name='', domain_var=domain_var)
    for f in factors:
        to_move = np.arange(len(f.scope))
        args = [slice(None)] * f.value.ndim + [np.newaxis] * \
            (sumpot.value.ndim-f.value.ndim)
        new_pos = [sumpot.scope.index(i) for i in f.scope]
        vvv = np.moveaxis(f.value[tuple(args)], to_move, new_pos)
        sumpot.value += vvv

    return sumpot


def max_product_eliminate_variable(factors, variable):
    """Eliminiate variable V from the set of factors."""
    if DEBUG:
        logger.debug('Eliminate {}'.format(variable))
    # Combine all the factors that contain i in their scope.
    # Remove them.
    factors_with_i = [f for f in factors if variable in f.scope]
    factors_without_i = [f for f in factors if f not in factors_with_i]

    # the message factor.
    if len(factors_with_i) > 1:
        message = factor_sum(factors_with_i)
    else:
        message = factors_with_i[0]

    # the max-marginalised message.
    new_factor = max_marginalise(message, variable)
    return factors_without_i + [new_factor],  message


def traceback_map(order, messages):
    """Traceback the maximum a posteriori factor to make the actual inference"""
    x = {}
    xout = {}

    for i, m in zip(order, messages):
        args = [x[var] if var != i else slice(None, None) for var in m.scope]
        x[i] = np.argmax(m.value[tuple(args)])
        xout[i] = m.domain_var[i][x[i]]

    # for k in x.keys():
    #    print('Variable number {} amax={} max={}'.format(k, x[k], xout[k]))
    return xout


def max_marginalise(factor, variable):

    scope = factor.scope.copy()
    marginalise_on_pos = scope.index(variable)
    scope.pop(marginalise_on_pos)
    domain_var = {k: factor.domain_var[k] for k in scope}
    mxmargin = Potential(scope, letter='M', domain_var=domain_var, mx=True)
    mxmargin.value = np.max(factor.value, marginalise_on_pos)
    mxmargin.argmax_pos = np.argmax(factor.value, marginalise_on_pos)

    # print('factor {} {}, marginalise on {}'.format(factor,factor.value.shape, variable))
    # print('Domain of {} : {}'.format(variable, factor.domain_var[variable]))
    # print('max value: {}'.format(mxmargin.value))
    # print('Argmax_position: {}'.format(mxmargin.argmax_pos))

    if mxmargin.argmax_pos.ndim:
        mxmargin.argmax = [factor.domain_var[variable][x]
                           for x in mxmargin.argmax_pos]
    else:
        mxmargin.argmax = factor.domain_var[variable][mxmargin.argmax_pos]
    return mxmargin


def log_likelihood(theta, config, factor_generator, ordering):
    factors = factor_generator(theta, {k: [v] for k, v in config.items()})
    _, ret, _ = max_product_variable_elimination(
        factors, ordering, backtrack=True)
    return ret[0].value


def expectation_maximisation(factor_generator, domain_var, ordering, theta, theta_min=0, theta_max=1, mxsteps=10, tol=1e-8, init_config=None):
    """
    factor_genetator: (func) theta, domain_var -> list of factors
    ordering: (list) variable elinination order.
    theta: (float) initial theta
    """
    start_time = time.time()
    theta_list = [theta, ]
    lk_list = [None, ]
    step = 0
    converged = False
    if not(theta_min < theta < theta_max):
        logger.warning('Initial parameter {} out of bound ({}, {}). Clipped.'.format(
            theta, theta_min, theta_max))
        theta = np.clip(theta, theta_min, theta_max)
    while step <= mxsteps and not converged:
        logger.info(
            '{:.2f} | E-M STEP {}/{}'.format(time.time()-start_time, step, mxsteps))
        # EXPECTATION Find the MAP configuration of the network for the current theta
        if step == 0 and init_config is not None:
            map_config = {}
            for k, v in init_config.items():
                if v not in domain_var[k]:
                    idx = domain_var[k].searchsorted(v)
                    map_config[k] = domain_var[k][np.clip(
                        idx, 0, len(domain_var[k])-1)]
                    logger.debug('Initial value of {} set from {} to {} to fit domain'.format(
                        k, v, map_config[k]))
        else:
            logger.info(
                '{:.2f} | E STEP {}/{}'.format(time.time()-start_time, step, mxsteps))
            factors = factor_generator(theta_list[-1], domain_var)
            logger.info('{:.2f} | factor generated'.format(
                time.time()-start_time))
            map_config, _, _ = max_product_variable_elimination(
                factors, ordering, backtrack=True)
            logger.info('{:.2f} | MPVE done'.format(time.time()-start_time))
            logger.debug('End of Expectation step {}. \n Here are the values {}'.format(
                step, Counter(map_config.values())))

        logger.info('{:.2f} | M STEP {}/{}'.format(time.time() -
                                                   start_time, step, mxsteps))
        # MAXIMISATION: find the theta that optimise the likelihood for the current MAP config.
        llkhd = partial(log_likelihood, config=map_config,
                        factor_generator=factor_generator, ordering=ordering)
        res = scipy.optimize.minimize_scalar(
            lambda x: -llkhd(x), bounds=(theta_min, theta_max), method='bounded')
        theta_list.append(res.x)
        lk_list.append(llkhd(res.x))
        converged = np.isclose(theta_list[-1], theta_list[-2], atol=tol)
        logger.info('End of Maximisation step {}. theta={} maximize the log-likelihood={} {}'.format(
            step, theta_list[-1], lk_list[-1], 'CONVERGED !' if converged else ''))
        step += 1
    factors = factor_generator(theta_list[-1], domain_var)
    map_config, _, _ = max_product_variable_elimination(
        factors, ordering, backtrack=True)
    return theta_list, lk_list, map_config


if __name__ == "__main__":
    test()
