"""interface.py - A small unefficient tree library.
This file is part of the coalgen package.
Copyright 2019 Guilhem Doulcier, Licence GNU AGPL3+

Please do not judge me, person of the future. 

Nodes are dictionary with (at least) two entries, `order`, which serve
as an id for the node, and `children` wich is a list of nodes
objects. Trees are represented by their root node. 
"""
from collections import defaultdict
import logging
import numpy as np
import pandas as pd
logger = logging.getLogger('coalgen')

def unroll(tree):
    """ Convert the tree to a flat list.  """
    out = [(tree['order'], [child['order'] for child in tree['children']] )]
    for child in tree['children']:
            out += unroll(child)
    return out

def get_attr(tree, name, default=0):
    """ Get attributes of nodes as a dictionary. """
    out = {}
    try:
        out[tree['order']] = tree[name]
    except KeyError:
        logger.warning('Node {} set to {} when extracting {}'.format(tree['order'],default, name))
        out[tree['order']] = default

    for child in tree['children']:
            out.update(get_attr(child,name))
    return out

def set_attr(tree, name, values):
    """Set attribute name (str) to values (dict) in tree."""
    logger.debug('Setting {} of {} to  {}'.format(name, tree['order'], values[tree['order']]))
    tree[name] = values[tree['order']]
    for child in tree['children']:
        set_attr(child, name, values)

def parents(tree, key='order'):
    """Get a dict that maps each node to its parent. Nodes are identified
    by their `key` attribute."""
    out = {child[key]:tree[key] for child in tree['children']}
    for child in tree['children']:
            out.update(parents(child, key=key))
    return out

def get_name(tree, default='root'):
    out = {}
    try:
        out[tree['order']] = '{}_{}.{}'.format(tree['tube'],tree['cycle'],tree['phase'])
    except KeyError:
        print('Node {} set to {} when extracting {}'.format(tree['order'],default,'name'))
        out[tree['order']] = default
    for child in tree['children']:
            out.update(get_name(child,default=default))
    return out

def set_elimination_order(tree: dict) -> None:
    """ Set elimination order for the Bayesian Network Optimisation algorithm"""
    depth_count = defaultdict(lambda: 0)
    def set_depth(node: dict, level: int = 0):
        node['depth'] = level
        depth_count[level] += 1
        for child in node['children']:
            set_depth(child, level+1)
    set_depth(tree)
    offset = {k:int(np.sum([nb for d, nb in depth_count.items() if d > k]))
              for k in depth_count.keys()}
    def set_order(node: dict):
        node['order'] = depth_count[node['depth']] + offset[node['depth']]
        depth_count[node['depth']] -= 1
        for child in node['children']:
            set_order(child)
    set_order(tree)

def count(tree):
    """ Count the number of nodes in the tree """
    number = 0
    def count_node(node):
        nonlocal number
        number += 1
        for child in node['children']:
            count_node(child)
    count_node(tree)
    return number

class DefaultNamer:
    def __init__(self):
        self.x = 0
    def __call__(self,*args):
        self.x += 1
        return 'N{}'.format(self.x)

def to_df(tree, name_node=DefaultNamer()):
    """ Convert a tree to a dataframe """
    data = []
    def read(node,parent_id=None):
        id_node = name_node(node)
        if parent_id is not None:
            data.append({k:v for k,v in node.items() if k!='children'})
            data[-1]['id'] = id_node
            data[-1]['parent_id'] = parent_id
        for child in node['children']:
            read(child,id_node)
    read(tree)
    return pd.DataFrame(data).set_index('id',drop=False).sort_values(by='id')

def from_df(df, name=None):
    """ Convert a dataframe to a tree """ 
    nodes = {name:v for name,v in zip(df.index, df.to_dict(orient='records'))}
    tree = {'name':name, 'children':[]}
    for n in nodes.values():
        n['children'] = []
    for n in nodes.values():
        if n['parent_id'] in nodes:
            nodes[n['parent_id']]['children'].append(n)
        else:
            tree['children'].append(n)
    return tree
