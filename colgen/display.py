""" display.py : a module used to produce static plots of collective genealogies.
Works well with jupyter notebooks.

This file is part of the coalgen package.
Copyright 2019 Guilhem Doulcier, Licence GNU GPL3+
"""
import logging

from collections import defaultdict, deque

import numpy as np
import pandas as pd
import os

import matplotlib.pyplot as plt
from matplotlib.patches import Circle, PathPatch
from matplotlib.collections import PatchCollection
from matplotlib.path import Path
import matplotlib.cm as cm
import matplotlib.colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import re

logger = logging.getLogger('coalgen')

COLORS = [(0x4e/255, 0x79/255, 0xa7/255),
          (0xf2/255, 0x8e/255, 0x2b/255),
          (0xe1/255, 0x57/255, 0x59/255),
          (0x59/255, 0xa1/255, 0x4f/255),
          (0xb0/255, 0x7a/255, 0xa1/255),
          (0xba/255, 0xb0/255, 0xac/255)]


def layout(branches, sortby=None, root='ROOT', delroot=True):
    """Compact layout (position x and y) of the nodes of the tree.

    Args:
        branches (list): a list of [parent, child] elements.
        sortby (dict): a mapping node->sorting index.
        root (str): root node name.
        delroot (bool): if true, remove the root of the tree"""
    height = {root: 0}
    width = {root: 0}
    current = root
    if sortby is None:
        def sortf(x): return 1
    else:
        def sortf(k): return sortby[k]
    to_visit = deque()
    width_at_height = defaultdict(lambda: 0)
    while current is not None:
        children = sorted([b for b in branches if b[0] == current],
                          key=lambda b: sortf(b[1]))
        for parent, child in children:
            to_visit.append(child)
            height[child] = height[parent] + 1
            width_at_height[height[child]] += 1
            width[child] = width_at_height[height[child]]
        try:
            current = to_visit.popleft()
        except IndexError:
            current = None
    if delroot:
        del height[root]
        del width[root]
    return height, width


def load_df(dataframe):
    """Load a csv file and compute the layout

    Args:
        dataframe (path as str or pandas.DataFrame): table containing
            a column "parent" and a column "name".
    Return:
        dict used in draw_tree, and the dataframe.
    """
    if isinstance(dataframe, str):
        dataframe = pd.read_csv(dataframe)

    branches = [(x['parent'], x['name']) for i, x in dataframe.iterrows()]
    root = [x for x in dataframe.parent if 'ROOT' in x][0]
    nodes = dataframe.name.unique()
    xinfo, yinfo = layout(branches, sortby={x: x for x in nodes}, root=root)
    return {'branches': branches, 'nodes': nodes, 'xinfo': xinfo, 'yinfo': yinfo}, dataframe


def coalescent(nodes, branches):
    """Compute the coalescent of some nodes.

    Given a list of nodes and a list of branches, return the set of
    nodes in the coalescent tree of the input.
    """
    parent_map = {child: parent for parent, child in branches}
    coal = set(nodes)
    parents = set(nodes)
    while len(parents):
        parents = {parent_map[n] for n in parents if n in parent_map}
        coal = coal.union(parents)
    return coal


def descent(nodes, branches):
    """Compute the descent of some nodes.

    Given a list of nodes and a list of branches, return the set
    of nodes in the descent tree of the input.
    """
    desc = set(nodes)
    children = set(nodes)
    while len(children):
        children = {child for parent, child in branches if parent in children}
        desc = desc.union(children)
    return desc


def draw_tree(branches, xinfo, yinfo, color=None, oinfo=None,
              width=2048, height=1200, dpi=160, zoom=1, size=12, lw=2, pad_cax=0.4,
              child_color_branch=False,
              low_alpha=0.1, colormap=False, marked=None, ax=None):
    """ Draw the genealogy.

    Args:
        branches (list): list of links as follow: (name parent, name child)
        xinfo (dict): mapping name->position in x.
        yinfo (dict): mapping name->position in y.
        color (dict): mapping name->color in rgb (or number if colormap==True)
        oinfo (dict, str): mapping name->opacity (in [0,1]), if "coal" highlight the coalescent of the tree.
        width (int): width of the plot (px)
        height (int): height of the plot (px)
        dpi (int): dot per inches. (Used only if ax is None).
        zoom (float): scaling of nodes an linewidth.
        size (float): size of a node (px)
        lw (float): line width (px)
        low_alpha (float): value of alpha if oinfo is 'coal' or 'child'.
        colormap (bool): if true, convert integers to rgb in color dict.
        ax (matplotlib.Axes): matplotlib axis to plot on.
    Return:
        (matplotlib axe, (xscale,yscale,oscale))
    """
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(zoom*width/dpi, zoom*height/dpi), dpi=dpi)
    else:
        fig = ax.get_figure()

    lw = lw*zoom
    size = size*zoom

    childs = frozenset([b[1] for b in branches])
    parents = frozenset([b[0] for b in branches])
    nodes = parents.union(childs)

    xmin = np.min(np.array([*xinfo.values()]))
    xmax = np.max(np.array([*xinfo.values()]))
    ymin = np.min(np.array([*yinfo.values()]))
    ymax = np.max(np.array([*yinfo.values()]))
    leaves = [n for n, x in xinfo.items() if x == xmax]

    if oinfo is None:
        oinfo = {n: 1 for n in nodes}
    elif oinfo == 'child':
        oinfo = {n: 1 if n in parents or n in leaves else low_alpha for n in nodes}
    elif oinfo == 'coal':
        coal = coalescent(leaves, branches)
        oinfo = {n: 1 if n in coal else low_alpha for n in nodes}

    if color is None:
        color = {n: (0, 0, 0) for n in nodes}
    elif colormap is True:
        nb = np.array([x for k, x in color.items() if k in nodes])
        mx, mi = nb.max(), nb.min()
        color = {k: cm.viridis((float(v)-mi)/(mx-mi))[:3] for k, v in color.items()}
        norm = matplotlib.colors.Normalize(vmin=mi, vmax=mx, clip=False)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='1%', pad=pad_cax)
        fig.colorbar(cm.ScalarMappable(norm=norm, cmap='viridis'), cax=cax)

    margin = 3*size
    def xscale(k): return (width-margin)*(xinfo[k]-xmin)/(xmax-xmin) + 0.5*margin
    def yscale(k): return (height-margin)*(yinfo[k]-ymin)/(ymax-ymin) + 0.5*margin
    def oscale(k): return oinfo[k] if k in oinfo else 1
    nodes_patches = []

    for parent, child in branches:
        color_branch = child if child_color_branch else parent
        try:
            p = PathPatch(
                Path([(xscale(parent)+size, yscale(parent)),
                      ((xscale(child)+xscale(parent))/2, yscale(parent)),
                      ((xscale(parent)+xscale(child))/2, yscale(child)),
                      (xscale(child)-size, yscale(child))],
                     [Path.MOVETO, Path.CURVE4, Path.CURVE4, Path.CURVE4]),
                fc="none",
                lw=lw,
                ec=color[color_branch] if color_branch in color else (.3, .3, .3),
                alpha=oscale(child))
            nodes_patches.append(p)
        except KeyError:
            if 'ROOT' not in parent:
                logger.error('keyerror {} {}'.format(parent, child))

    for node in nodes:
        try:
            nodes_patches.append(Circle((xscale(node)+0.1*size, yscale(node)+0.1*size),
                                        0.8*size,
                                        ec='none',
                                        fc=color[node] if node in color else (.3, .3, .3),
                                        alpha=oscale(node)))
        except KeyError:
            if 'ROOT' not in node:
                logger.error('keyerror {}'.format(node))

    p = PatchCollection(nodes_patches, match_original=True)
    ax.add_collection(p)

    yticklabels, yticks = tick_generator(yinfo, yscale)
    xticklabels, xticks = tick_generator(xinfo, xscale)
    ax.set(yticks=yticks,
           yticklabels=yticklabels,
           xticks=xticks,
           xticklabels=xticklabels,
           xlim=(0, width),
           ylim=(0, height))
    ax.tick_params(axis='y', which='both', labelleft='on', labelright='on')

    if marked:
        ax.scatter([xscale(x) for x in marked],
                   [yscale(x) for x in marked],
                   fc='w', ec='k', marker='o')
    return ax, (xscale, yscale, oscale)


def tick_generator(coord_dict, scale):
    """Return the tickslabels and tick positions for the tree.

    The labels are generated by taking the longest substring which
    identify all the nodes that correspond to the tick, while removing
    the substring that are in common for all ticks.

    Args:
        coord_dict (dict): a mapping name->value.
        scale (func): a function that maps value->tick position.

    """
    ncord = len(frozenset([scale(x) for x in coord_dict.keys()]))

    # Remove the common prefix at all the categories.
    prefix = {}
    for order in [1, -1]:
        common_prefix = os.path.commonprefix([str(x)[::order] for x in coord_dict.keys()])
        j = len(common_prefix)
        ticklabels, ticks = zip(*frozenset([('', scale(k)) for k in coord_dict.keys()]))
        i = j
        while len(ticklabels) == ncord:
            try:
                ticklabels, ticks = zip(
                    *frozenset([(k[::order][j:i][::order], scale(k)) for k in coord_dict.keys()]))
            except Exception:
                tickslabels, ticks = [], []
            i += 1
        prefix[order] = (j, max(j, i-2))
    order = 1 if (prefix[1][1]-prefix[1][0]) > (prefix[-1][1]-prefix[-1][0]) else -1
    ticklabels, ticks = zip(
        *frozenset([(k[::order][prefix[order][0]:prefix[order][1]][::order].strip(), scale(k)) for k in coord_dict.keys()]))

    # Remove the common prefix between ticks.
    common_prefix = os.path.commonprefix(ticklabels)
    common_suffix = os.path.commonprefix([x[::-1] for x in ticklabels])[::-1]
    ticklabels = [x.replace(common_prefix, '').replace(common_suffix, '')
                  for x in ticklabels]

    return ticklabels, ticks


def plot_coalescent_of(branches, xinfo, yinfo, target_nodes=None, **kargs):
    """Plot the coalescent of target_nodes list. Extra parameters are passed to draw_tree"""
    coal = coalescent(target_nodes, branches)
    nodes = {x[0] for x in branches}.union({x[1] for x in branches})
    oinfo = {k: 1 if k in coal else 0.4 for k in nodes}
    fig = draw_tree(branches=branches,
                    xinfo=xinfo,
                    yinfo=yinfo,
                    oinfo=oinfo,
                    **kargs)
    return fig


def plot_descent_of(branches, xinfo, yinfo, target_nodes=None, **kargs):
    """Plot the descent of target_nodes list. See the documentation of draw_tree for the other parameters"""
    desc = descent(target_nodes, branches)
    nodes = {x[0] for x in branches}.union({x[1] for x in branches})
    oinfo = {k: 1 if k in desc else 0.4 for k in nodes}
    fig = draw_tree(branches=branches,
                    xinfo=xinfo,
                    yinfo=yinfo,
                    oinfo=oinfo,
                    **kargs)
    return fig
