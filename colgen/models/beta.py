""" Beta distribution model
This file is part of the coalgen package.
Copyright 2020 Guilhem Doulcier, Licence GNU GPLv3+
"""
from functools import partial
import scipy.stats
import scipy.special
import scipy.integrate
import numpy as np
import pytest


def model(t, parent, is_extinct, sigma, grid):
    """Return P(X=t|X_p_i=parent)P(E=is_extinct|X=t)

    t: trait of the focal node
    parent: trait of the parental node
    is_extinct: true if the node is extinct
    sigma: confidence
    grid: domain size
    """
    if not sigma:
        return 1/(2*grid**2)
    alpha = 1+parent*sigma
    beta = 1+(1-parent)*sigma
    k = 0 if is_extinct else 1
    rv = scipy.stats.beta(alpha+k, beta+1-k)
    dx = 1/(grid*2)
    a = t-dx
    b = t+dx
    proba = rv.cdf(b)-rv.cdf(a)
    scaling = (scipy.special.beta(alpha, beta) /
               scipy.special.beta(alpha+k, beta+1-k))
    return proba / (scaling * grid)


def get_domain(steps):
    dx = 1/(steps*2)
    return np.linspace(0+dx, 1-dx, steps)


