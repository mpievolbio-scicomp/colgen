""" Jump Model 
This file is part of the coalgen package.
Copyright 2020 Guilhem Doulcier, Licence GNU GPLv3+
"""
from functools import partial
import numpy as np
import pytest


def model(trait, parent, is_extinct, sigma, grid):
    """Return P(X=t|X_p_i=parent)P(E=is_extinct|X=t)

    t: trait of the focal node
    parent: trait of the parental node
    is_extinct: true if the node is extinct
    sigma: probability that the parent is the same than its child.
    grid: domain size
    """
    proba_trait_knowing_parent = ((sigma + (1 - sigma)/grid)
                                  if trait == parent
                                  else (1 - sigma)/grid)
    prob_extinct_knowing_trait = 1-trait if is_extinct else trait
    return float(proba_trait_knowing_parent*prob_extinct_knowing_trait)/grid


def get_domain(grid):
    return np.linspace(0, 1, grid)[1:-1]
