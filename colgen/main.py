"""survival - survival estimation
This file is part of the coalgen package.
Copyright 2019 Guilhem Doulcier, Licence GNU AGPL3+
"""
import os
from functools import lru_cache, partial
import logging

import numpy as np
import scipy.integrate
import scipy.stats
import pandas as pd

import colgen.bn_discrete as bn
import colgen.dataimport
import colgen.tree

logger = logging.getLogger('coalgen')

def load_tree(file):
    if isinstance(file, str):
        dataframe = pd.read_csv(file)
        name = os.path.basename(file)
        logger.info('Reading {}'.format(file))
    else:
        dataframe = file.copy()
        name = 'data'

    data = colgen.dataimport.table_to_json(dataframe, name, dump=False)
    logger.info('Elimination order setting...')
    colgen.tree.set_elimination_order(data)

    odict = {node: order for order, node in colgen.tree.get_attr(data, 'name').items()}
    dataframe['order'] = [odict[n] for n in dataframe.name]
    logger.debug('DATAFRAME:\n{}'.format(dataframe))

    extinct = dataframe.set_index('order').extinct.to_dict()
    names = dataframe.set_index('order')['name'].to_dict()
    parents = colgen.tree.parents(data)
    survival_rate_root = len(
        [x for x in data['children'] if not extinct[x['order']]])/len(data['children'])
    ndead = np.sum(list(extinct.values()))
    logger.debug("{}/{} ~ {:%} are extinct".format(ndead,
                                                   len(extinct),
                                                   ndead/len(extinct)))

    if "parent" not in dataframe:
        dataframe['parent'] = [parents[o]
                               if o in parents else '' for o in dataframe.order]

    def get_init_survival(name, df):
        clutch = df.query('parent=="{0}"|name=="{0}"'.format(name))
        return 1 - (clutch.extinct.sum() / clutch.shape[0])
    initial_config = {o: get_init_survival(n, dataframe)
                      for n, o in zip(dataframe.name, dataframe.order)}

    return {'data': data,
            'dataframe': dataframe,
            'extinct': extinct,
            'initial_config': initial_config,
            'order': list(range(1, len(odict)+1)),
            'names': names,
            'root_trait': survival_rate_root,
            'parents': parents}


def factor_generator_generator(data, potential, default_domain=None):
    def factor_generator(sigma_value, domain_var):
        factors = []
        for node, is_extinct in sorted(data['extinct'].items()):
            if node in data['parents']:
                factors.append(bn.Potential([node, data['parents'][node]],
                                            lambda trait, parent, extinct=is_extinct, sigma=sigma_value: potential(
                                                trait, parent, extinct, sigma=sigma),
                                            name='Potential of {}'.format(
                                                data['names'][node]),
                                            default_domain=default_domain,
                                            domain_var=domain_var))
            else:
                factors.append(bn.Potential([node],
                                            lambda trait, parent=data['root_trait'],
                                            extinct=is_extinct,
                                            sigma=sigma_value: potential(trait, parent, extinct, sigma=sigma),
                                            name='Potential of {}'.format(
                                                data['names'][node]),
                                            default_domain=default_domain,
                                            domain_var=domain_var))
        return factors
    return factor_generator


def fit(csv, model,  sigma_range, max_em_steps=10, steps=8):
    pot, dom = colgen.models.generate_log_potential_and_domain(model, steps)
    tree  = load_tree(csv)

    fg = factor_generator_generator(tree, partial(pot, grid=steps), default_domain=dom)
    dv = {k:dom for k in tree['order']}
    sigma_conv, lklhood_conv, map_config = bn.expectation_maximisation(factor_generator=fg,
                                                                       domain_var=dv,
                                                                       ordering=tree['order'],
                                                                       theta=sigma_range[0],
                                                                       theta_min=sigma_range[1],
                                                                       theta_max=sigma_range[2],
                                                                       mxsteps=max_em_steps,
                                                                       init_config=tree['initial_config'])
    tree['dataframe']['survival_estimate'] = [map_config[i] for i in tree['dataframe'].order]
    convergence = pd.DataFrame({'likelihood':lklhood_conv, 'sigma':sigma_conv})
    return tree['dataframe'], convergence
