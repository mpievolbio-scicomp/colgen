# Collective Genealogies visualisation and analysis

![Screenshot of a tree](./doc/img/screenshot02.png)

Colgen is a python package to visualise and analyse collective-level genealogies. Its features include:

- Visualise propagation-replacement data in the form of a tree where each node correspond to a collective, interactively (served locally as HTML) or statically (using the python API).
- Build a Binary-state Bayesian Network representation of the genealogy where each node display (1) or not (0) a mutation. Use a Maximum-a-posteriori method to infer probable the location of mutations in the genealogy (from a few sequenced points).
- Build a Continuous-state Bayesian Network representation of the genealogy where each node is associated with a survival probability. Use a Maximum-a-posteriori method to infer locations in the genealogy where this probability changes.  

## Citing Colgen 

You can cite Colgen as: 

> Doulcier, G. (2019). Colgen, Collective Genealogies Visualisation and Analysis. Zenodo. DOI: 10.5281/zenodo.7342768.

## Installation

If you have Python 3 along with `pip`, installing the current
development version of `colgen` can be done by:

```
pip install https://gitlab.com/ecoevomath/colgen/-/archive/master/colgen-master.zip
```

## Quick Start

Colgen exposes a command line interface to analyse and visualise
collective-level genealogies. There are three main commands.

- `interactive`: Start an interactive visualisation of the collective-level genealogy in the browser.
- `mutation`: Perform mutation propagation using the Bayesian network model.
- `survival`: Perform survival probability estimation using the Bayesian network model.

Detailed instructions about the syntax can be obtained with `colgen
--help` and `colgen command --help`.

### Data Format

A collective-level genealogy must be inputed as a csv file. The file
`data/dummy_tree.csv` offers an example.  The following columns are
required:

- `name` : A string uniquely identifying the collective.
- `parent`: A string giving the identifier of the parent collective.

### Interactive visualisation

![Example of interactive visualisation](./doc/img/screenshot01.png)

To start an interactive visualisation of a tree file, use:

```
colgen interactive dummy_tree.csv --browser
```

This will open your web browser and display the tree.

At the top of the window is the control panel. If you gave several files names to the command (as a list or a wildcard `colgen interactive *.csv`), you can use the drop-down menu at the top to change file. You can also adjust the coloration of the nodes, and the size of the tree display.

Click on the "+" at the bottom of the screen to display a table with all the nodes built from the csv file. The table is dynamically filtered according to your selection.

Click once on a node to select its descent and ancestor, click twice to select only the descent, click a third time to select only the ancestry.


## Mutation mapping

```
usage: colgen mutation [-h] [--mu [MU]] [--mu_r [MU_R]] [--p0 [P0]] [--p1 [P1]]
					   [--out_dir [OUT_DIR]] [--suffix [SUFFIX]]
					   [files [files ...]]

Mutation mapping

positional arguments:
  files                Input tree

optional arguments:
  -h, --help           show this help message and exit
  --mu [MU]            Mutation probability (default: 0.001).
  --mu_r [MU_R]        Mutation reversion probability (default: 0.0001).
  --p0 [P0]            Probability of observing a non existing mutation
					   (default: 0).
  --p1 [P1]            Probability of observing an existing mutation (default:
					   1).
  --out_dir [OUT_DIR]  Output folder (default: .).
  --suffix [SUFFIX]    Mutation file suffix (default: _mutations).
```

For a `tree.csv` input, the software will look for a `tree[SUFFIX].csv`
file containing the mutation data. The file
`data/dummy_tree_mutations.csv` offers an example. Mutation data must
be a csv file with at least two columns:

- `node`, a string identifying the culture sequenced
- `mutation` a string identifying the mutation.

The output is divided in two files:

- `tree_mutation_mapping.csv` is a double entry table with nodes and
  mutations, with the Maximum a Posteriori estimate of the presence of
  a mutation (`true` or `false`). A column `sequenced` is added that
  has value `true` if the node was present at least once in the input
  mutation file.
- `tree_mutation_report.csv` is a table with one line per mutation
  that gives the number of sequenced, and total nodes carrying this
  mutation, as well as the parameters and output of the Bayesian model.

## Survival probability estimation

```
usage: colgen survival [-h] [--sigma0 [SIGMA0]] [--sigma_max [SIGMA_MAX]] [--sigma_min [SIGMA_MIN]]
              [--max_em_steps [MAX_EM_STEPS]] [--steps [STEPS]] [--outpath [OUTPATH]] [--model [MODEL]]
              [-v]
              [files ...]

Survival estimation

positional arguments:
  files                 Input tree

optional arguments:
  -h, --help            show this help message and exit
  --sigma0 [SIGMA0]     Initial sigma
  --sigma_max [SIGMA_MAX]
                        Maximal value of sigma
  --sigma_min [SIGMA_MIN]
                        Minimal value of sigma
  --max_em_steps [MAX_EM_STEPS]
                        Max number of steps
  --steps [STEPS]       Grid size
  --outpath [OUTPATH]   Output path
  --model [MODEL]       Heredity Model (available models are truncnorm and jump)
  -v, --verbose         Verbosity level (default=info, -v=debug)
```

Tree data must be a csv file with at least three columns:

- `name` : A string uniquely identifying the collective.
- `parent`: A string giving the identifier of the parent collective.
- `extinct`, a boolean value indicating if the node died (True) or not (False).

The output is divided in two files:

- `[grid]_[model]_tree.csv` is a copy of the tree dataset, with a new column "survival_estimate".
- `[grid]_[model]_tree.convergence` is a table where each line correspond to a E-M step. The columns give the parameter of the model, and the associated likelihood at this step.

See more examples of fit and ways to display them in `doc/example_survival.ipynb`.

## Use as a library

The `colgen.display` module is designed to be used in script or
jupyter notebook. Here is an example:

```python
import colgen.display

# Load the dataset (you can provide a path to a csv file or a pandas.DataFrame object),
# the only two required columns are "name" and "parent".
tree,df = colgen.display.load_df('../data/processed/WT-L_auckland.csv')

# Display the tree (in black)
ax, scales = colgen.display.draw_tree(tree['branches'], tree['xinfo'], tree['yinfo'])
ax.set_title('My tree') # ax is a matplotlib axis object.
ax.get_figure().savefig('my_tree.png')

# You can highlight the coalescent of the whole tree:
ax, scales = colgen.display.draw_tree(tree['branches'], tree['xinfo'], tree['yinfo'], oinfo="coal")

# You can provide a colour for the node with the color argument, and an opacity with the oinfo argument.
color = {x['name']: 'C0' if "P1" in x['name'] else 'C1' for k,x in df.iterrows()}
fig = colgen.display.draw_tree(tree['branches'], tree['xinfo'], tree['yinfo'], color=color)

# You can highlight the coalescent tree or the descent tree of a set of nodes
fig = colgen.display.plot_coalescent_of(tree['branches'], tree['xinfo'], tree['yinfo'],
                                        target_nodes=['WTL_C09P1_F1', 'WTL_C09P1_F8'])
fig = colgen.display.plot_descent_of(tree['branches'], tree['xinfo'], tree['yinfo'],
       				 target_nodes=['WTL_C00P1_F1', 'WTL_C00P1_F8'])
```

See more examples in `doc/example_notebook.ipynb`.

## Complete Dependencies

This software is written in Python 3 and uses the following packages for data processing:

- **[numpy](http://www.numpy.org/)**: general scientific computing library.
- **[scipy](http://scipy.org/)**: signal smoothing (convolution) and model fitting.
- **[pandas](http://pandas.pydata.org/)**: dataframe implementation.
- **[matplotlib](http://matplotlib.org/)**: static figures generation.
- **[cherrypy](http://cherrypy.org/)**: webserver and application framework.

Data visualization is made in the browser using the following
libraries (included in the `colgen/web/static/lib` directory):

- **[jquery](https://jquery.com/)** Javascript framework.
- **[d3.js](https://d3js.org/)**: Linking data to the DOM.
- **[bootstrap](https://getbootstrap.com/)**: Clean looking CSS.

## License

Copyright (c) 2019 Guilhem Doulcier

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
