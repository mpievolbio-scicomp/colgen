""" Test mutation propagation"""
import numpy as np
import pandas as pd
from colgen.mutation import generate_potential, max_sum_ve, sum_product_ve, main

def test_stable():
    """ A simple test of the mutation propagation (not exhaustive). Test that sequenced mutations are in the mapping."""
    nd = ['node1','node2','node3']
    node_table = pd.DataFrame({'name': nd,
                               'parent': ['ROOT','node1','node1']})
    mutation_table = pd.DataFrame({'node': ['node1','node2','node2','node2'],
                               'mutation': ['mut1','mut2','mut3','mut1']})
    report, mapping = main(node_table, mutation_table, 0.001, 1e-9, 1e-9, 1-1e-9, )
    print(mapping)
    assert all(mapping.sequenced == pd.Series([True, True ,False], index=nd)), 'sequenced incorrect'
    assert mapping.loc['node1','mut1'] == True
    assert mapping.loc['node2','mut2'] == True
    assert mapping.loc['node2','mut3'] == True
    assert mapping.loc['node2','mut1'] == True

    
def test_generate():
    gene = generate_potential(2, {0:1, 1:1}, p =[.2,.7], mu=.1)
    fixture = np.array([[[ 0.2 ,  0.2 ], [ 0.7 ,  0.7 ]],
                        [[ 0.18,  0.02], [ 0.07,  0.63]]])
    np.testing.assert_allclose(gene, fixture)
    
    gene = generate_potential(2, {0:0, 1:0}, p =[.2,.7], mu=.1)
    fixture = np.array([[[ 0.8 ,  0.8 ], [ 0.3 ,  0.3 ]],
                        [[ 0.72,  0.08], [ 0.03,  0.27]]])
    np.testing.assert_allclose(gene, fixture)

def test_max_sum_ve():
    for ev, MAP, prob in [({0:0, 1:0}, [0, 0], 0.576),
                          ({0:0, 1:1}, [1, 1], 0.189),
                          ({0:1, 1:0}, [1, 1], 0.189),
                          ({0:1, 1:1}, [1, 1], 0.441)]:
        print('****** Evidence = {} ******'.format(ev))
        potential = generate_potential(2, ev, p = [.2,.7], mu=.1)
        r,s = max_sum_ve(potential, [(1, tuple()), (0,(1,))], {1:0})
        np.testing.assert_almost_equal(np.exp(s), prob)
        np.testing.assert_almost_equal(r, MAP)
        print("----> MaP: {}, exp(s): {}".format(r, np.exp(s)))

def test_sum_product_ve():
    for ev, MAP, prob in [([0, 0], [0, 0], 0.576),
                          ([0, 1], [1, 1], 0.189),
                          ([1, 0], [1, 1], 0.189),
                          ([1, 1], [1, 1], 0.441)]:
        potential = generate_potential(2, ev, p = [.2,.7], mu=.1)
        s = sum_product_ve(potential, MAP, [(1, tuple()), (0, (1,))], {1:0})
        np.testing.assert_almost_equal(np.exp(s), prob)
