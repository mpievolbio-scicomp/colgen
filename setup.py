from setuptools import setup

setup(name='colgen',
      version='0.1',
      description='Collective Genealogies Analysis',
      url='https://gitlab.com/ecoevomath/colgen',
      author='Guilhem Doulcier',
      author_email='guilhem.doulcier@ens.fr',
      license='GPLv3+',
      python_requires='>=3',
      packages=['colgen', 'colgen.web', 'colgen.models'],
      entry_points={
          'console_scripts': [
              'colgen = colgen.interface:cli',
          ]},
      classifiers=[
          'Development Status :: 4 - Beta',
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: GNU Affero General Public License v3',
          'Programming Language :: Python :: 3 :: Only',
          'Topic :: Scientific/Engineering :: Bio-Informatics',
          'Topic :: Scientific/Engineering :: Visualization'
      ],
      include_package_data=True,
      install_requires=[
          'numpy',
          'scipy',
          'pandas',
          'matplotlib',
          'cherrypy'],
      zip_safe=False)
